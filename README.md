# 2WD Platform

Konzola s dva motora i pripadajućim kotačima čini osnovu za kreativnu nadogradnju i upoznavanje djece s svijetom elektronike i automatike,

Konzola s dva motora dolazi s dba DC elektro motora, te pripadajucim kotačima, pomočnim kotačem, držačem baterija, prekidacem napajanja, te s svim pripadajucim montažnim elementima.

Osnovna stranica projekta nalazi se na http://pcb.daince.net/doku.php?id=2wd_project_v100

## Dodaci 

### Digitalni kontroler motora

Digitalni kontroler motora baziran je na MX1508 kontroleru s dva h-bridgea.

Više o kontroleru moguće je pronaći na https://gitlab.com/hztk/2wd-platform/tree/master/kontroler_motora

### Žični joystick

Jednostavni upravljač za digitalni kontroler motora kod kojega je veza ostvarena putem 10-žilnog IDC kabla.
  
Više o upravljaču moguće je saznati na https://gitlab.com/hztk/2wd-platform/tree/master/zicni_joystick